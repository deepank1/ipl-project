const csvFilePath1 = '../data/deliveries.csv'
const csvFilePath2 = '../data/matches.csv'
const csv = require('csvtojson')
const fs = require("fs");



csv()
    .fromFile(csvFilePath1)
    .then((deliveryObj) => {
        csv()
            .fromFile(csvFilePath2)
            .then((matchObj) => {


                let season = matchObj.reduce((acc, currentValue) => {
                    let index = acc.findIndex((element) => { return element.season === currentValue.season })
                    if (index < 0) {
                        acc.push({
                            season: currentValue.season,
                            matchIdArray: [currentValue.id],
                            players: []
                        })
                    } else {
                        if (!acc[index].matchIdArray.includes(currentValue.id)) {
                            acc[index].matchIdArray.push(currentValue.id)
                        }
                    }
                    return acc
                }, [])
                    .sort((a, b) => { return a.season - b.season })
                


                let result = deliveryObj.reduce((acc, currentValue) => {

                    let seasonIndex = season.findIndex((element) => {
                        return element.matchIdArray.includes(currentValue.match_id)
                    })
                    let playerIndex = acc[seasonIndex].players.findIndex((element) => {
                        return element.name === currentValue.batsman
                    })
                    if (playerIndex < 0) {
                        acc[seasonIndex].players.push({
                            name: currentValue.batsman,
                            total_runs: (Number(currentValue.total_runs)-Number(currentValue.extra_runs)),
                            total_balls: 1
                        })
                        
                    }
                    else {
                        acc[seasonIndex].players[playerIndex].total_runs += (Number(currentValue.total_runs)-Number(currentValue.extra_runs))
                        acc[seasonIndex].players[playerIndex].total_balls += 1
                    }
                    return acc
                }, season)
                .map((element)=>{
                    delete element.matchIdArray
                    element.players.map((ele)=>{
                        ele.strike_rate = ((ele.total_runs * 100) / (ele.total_balls)).toFixed(2)
                        return ele
                    })
                    return element
                })

                fs.writeFileSync("../public/output/7-strike-rate-for-batsman-per-season.json",JSON.stringify(result), 'utf-8', (err)=>{if(err) console.log})

            })
    })