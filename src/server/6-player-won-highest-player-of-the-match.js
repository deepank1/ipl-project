const csvFilePath='../data/matches.csv'
const csv=require('csvtojson')
const fs = require("fs");

csv()
.fromFile(csvFilePath)
.then((jsonObj)=>{

    let result = jsonObj.reduce((acc, currentValue)=>{
        let index = acc.findIndex((element)=>{return element.season === currentValue.season})
        if(index<0){
            acc.push({"season": currentValue.season,
                        "playerOfMatch":[{"playerName" : currentValue.player_of_match,
                                            "noOfTimes" : 1}]})
        }
        else{
            let playerIndex = acc[index].playerOfMatch.findIndex((ele)=>{return ele.playerName===currentValue.player_of_match})
            if(playerIndex<0){
                acc[index].playerOfMatch.push({"playerName" : currentValue.player_of_match,
                                                "noOfTimes" : 1 });
            }else{
                acc[index].playerOfMatch[playerIndex]["noOfTimes"] += 1;
            }
        }
        return acc;
    },[])
    .map((element)=>{

        let max = Math.max(...element.playerOfMatch.map((ele)=>{return ele.noOfTimes}))
        element.playerOfMatch = element.playerOfMatch.filter((ele)=>{return ele.noOfTimes === max});
        
        return element;
    })
    
    fs.writeFileSync("../public/output/6-player-won-highest-player-of-the-match.json",JSON.stringify(result), 'utf-8', (err)=>{if(err) console.log})
    
    })
    