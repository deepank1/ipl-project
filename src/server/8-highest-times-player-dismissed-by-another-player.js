const csvFilePath = '../data/deliveries.csv'
const csv = require('csvtojson')
const fs = require("fs");

csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {


        let result = jsonObj.reduce((acc, currentValue) => {
            if (currentValue.player_dismissed != "") {
                let index = acc.findIndex((element) => {
                    return element.batsman === currentValue.player_dismissed
                        && element.bowler === currentValue.bowler
                })
                if (index < 0) {
                    acc.push({
                        ["batsman"]: currentValue.player_dismissed,
                        ["bowler"]: currentValue.bowler,
                        ["dismisals"]: 1
                    });

                } else {
                    acc[index].dismisals += 1
                }
            }
            return acc
        },[])
            .sort((a, b) => {
                if (b.dismisals > a.dismisals) return 1
                else if (a.dismisals > b.dismisals) return -1
            });

        fs.writeFileSync("../public/output/8-highest-times-player-dismissed-by-another-player.json",JSON.stringify(result[0]), 'utf-8', (err)=>{if(err) console.log})


    })
