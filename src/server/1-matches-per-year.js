const csvFilePath='../data/matches.csv'
const csv = require('csvtojson')
const fs = require("fs");

csv()
.fromFile(csvFilePath)
.then((jsonObj)=>{

    let result = jsonObj.reduce((acc,currentValue)=>{
        if(!acc[currentValue.season]){
            acc[currentValue.season] = 0;
        }
        acc[currentValue.season]++;
        return acc
    },{});
    
    fs.writeFileSync("../public/output/1-matches-per-year.json",JSON.stringify(result), 'utf-8', (err)=>{if(err) console.log})
   
    })
    



