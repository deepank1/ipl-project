const csvFilePath1 = '../data/deliveries.csv'
const csvFilePath2 = '../data/matches.csv'
const csv = require('csvtojson')
const fs = require("fs");
const { cpuUsage } = require('process');



csv()
    .fromFile(csvFilePath1)
    .then((deliveryObj) => {
        csv()
            .fromFile(csvFilePath2)
            .then((matchObj) => {

                let year = 2015;

                let matchIdArray = matchObj.reduce((acc, currentValue)=>{
                    if (currentValue.season == year) {
                        acc.push(Number(currentValue.id))
                    }
                    return acc;
                },[]);

                let result =  deliveryObj.reduce((acc, currentValue)=>{
                    if (matchIdArray.includes(Number(currentValue.match_id))) {
                        let index = acc.findIndex((element)=>{return element.name === currentValue.bowler})
                        
                        if(index<0){
                            acc.push({name : currentValue.bowler,
                                    total_runs : Number(currentValue.total_runs),
                                    total_balls: 1});
                        }
                        else{
                            acc[index].total_runs += Number(currentValue.total_runs);
                            acc[index].total_balls += 1;
                        }
                        
                    }
                    return acc;
                },[])
                .map((element,index)=>{
                    element.economy = Math.round(element.total_runs*100/(element.total_balls/6))/100;
                    return element
                })
                .sort((a,b)=>{ if(a.economy>b.economy) return 1
                            else if (b.economy>a.economy) return -1})
                .slice(0,10);
                
                fs.writeFileSync("../public/output/4-top-10-economical-bowler-2015.json",JSON.stringify(result), 'utf-8', (err)=>{if(err) console.log})
                
        })
})