const fs = require("fs");
const csvFilePath = "../data/matches.csv"
const csv = require("csvtojson");

csv()
.fromFile(csvFilePath)
.then((jsonObj)=>{
    
    let result = jsonObj.reduce((acc, currentValue)=>{
        if(!acc[currentValue.winner]){
            acc[currentValue.winner] = { [currentValue.season]:1 }
        }
        else{
            if(!acc[currentValue.winner][currentValue.season]){
                acc[currentValue.winner][currentValue.season] = 1;
            }else{
                acc[currentValue.winner][currentValue.season]++;
            }
        }
        return acc;
    },{})

    fs.writeFileSync("../public/output/2-matches-won-per-team-per-year.json", JSON.stringify(result), 
    "utf-8",(err)=>{if(err) console.log(err)});
})