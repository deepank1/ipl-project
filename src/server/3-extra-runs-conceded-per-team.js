//Extra runs conceded per team in the required year

const csvFilePath1 = '../data/deliveries.csv'
const csvFilePath2 = '../data/matches.csv'
const csv = require('csvtojson')
const fs = require("fs");



csv()
    .fromFile(csvFilePath1)
    .then((deliveryObj) => {
        csv()
            .fromFile(csvFilePath2)
            .then((matchObj) => {

                let year = 2016;

                let matchIdArray = matchObj.reduce((acc, currentValue) => {
                    if (currentValue.season == year) {
                        acc.push(Number(currentValue.id));
                    }
                    return acc;
                }, []);

                let result = deliveryObj.reduce((acc, currentValue) => {
                    if (matchIdArray.includes(Number(currentValue.match_id))) {
                        if (!acc[currentValue.batting_team]) {
                            acc[currentValue.batting_team] = Number(currentValue.extra_runs)
                        }
                        else {
                            acc[currentValue.batting_team] += Number(currentValue.extra_runs)
                        }
                    }
                    return acc;
                }, {})

                fs.writeFileSync("../public/output/3-extra-runs-conceded-per-team-2016.json",JSON.stringify(result), 'utf-8', (err)=>{if(err) console.log})

            })
    })