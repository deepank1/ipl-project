const csvFilePath='../data/matches.csv'
const csv=require('csvtojson')
const fs = require("fs");

csv()
.fromFile(csvFilePath)
.then((jsonObj)=>{

    let result = jsonObj.reduce((acc, currentValue)=>{
        if(currentValue.toss_winner == currentValue.winner){
            if(!acc[currentValue.toss_winner]){
                acc[currentValue.toss_winner] = 1
            }
            else{
                acc[currentValue.toss_winner] += 1;
            }
        }
        return acc;
    },{})

    fs.writeFileSync("../public/output/5-team-won-toss-and-match.json",JSON.stringify(result), 'utf-8', (err)=>{if(err) console.log})

    
    })
    