const csvFilePath1 = '../data/deliveries.csv'
const csv = require('csvtojson')
const fs = require("fs")


csv()
    .fromFile(csvFilePath1)
    .then((deliveryObj) => {

        let result = deliveryObj.reduce((acc, currentValue) => {
            if (currentValue.is_super_over == 1) {
                let index = acc.findIndex((element) => { return element.name === currentValue.bowler })
                if (index < 0) {
                    acc.push({
                        name: currentValue.bowler,
                        total_runs: Number(currentValue.total_runs),
                        total_balls: 1
                    })
                }
                else {
                    acc[index].total_runs += Number(currentValue.total_runs)
                    acc[index].total_balls += 1
                }
            }
            return acc
        }, [])
            .map((element) => {
                element.economy = Math.round(element.total_runs * 100 / (element.total_balls / 6)) / 100
                return element
            })
            .sort((a, b) => {
                if (a.economy > b.economy) return 1
                else if (b.economy > a.economy) return -1
            })


        fs.writeFileSync("../public/output/9-bowler-with-best-super-over-economy.json", JSON.stringify(result[0]), 'utf-8', (err) => { if (err) console.log })


    })